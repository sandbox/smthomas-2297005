(function ($) {
  Drupal.behaviors.marketing_overlay = {
    attach: function (context, settings) {
      $('body').once('marketing-overlay', function() {
        if ($('#marketing-overlay-wrapper').length > 0) {
          
          // Display on page load if necessary.
          if (settings.marketing_overlay.display.pageload) {
            // Load the marketing_overlay_pageload cookie to track visits.
            var $pageload_cookie = readCookie('marketing_overlay_pageload');
            
            // Increment the pageload cookie so we can track the status.
            if (!$pageload_cookie) {
              createCookie('marketing_overlay_pageload', 1, 7);
            }
            else if ($pageload_cookie == 1) {
              createCookie('marketing_overlay_pageload', 2, 7);
            }
            
            if (settings.marketing_overlay.frequency == 'pageone' && !$pageload_cookie) {
              triggerOverlay(true);
            }
            else if (settings.marketing_overlay.frequency == 'pagetwo' && $pageload_cookie == 1) {
              triggerOverlay(true);
            }
            else if (settings.marketing_overlay.frequency == 'everypage') {
              triggerOverlay(true);
            }
            
          }
          
          // Display overlay when mouse is leaving page.
          if (settings.marketing_overlay.display.leaving) {
            //$(document).mouseleave(function() {
            //  triggerOverlay();
            //});
            // Load the marketing_overlay_hovering cookie to make sure overlay is only displayed once.
            
            addEvent(document, "mouseout", function(e) {
              e = e ? e : window.event;
              var from = e.relatedTarget || e.toElement;
              if (!from || from.nodeName == "HTML") {
                var $leaving_cookie = readCookie('marketing_overlay_leaving');
                if (!$leaving_cookie) {
                  createCookie('marketing_overlay_leaving', 1, 7);
                  triggerOverlay(false);
                }
              }
            });
          }
          
          // Display overlay when hovering itemed specified as the selector.
          if (settings.marketing_overlay.display.hovering) {
            var $css_selector = settings.marketing_overlay.selector;
            
            if ($css_selector) {
              $($css_selector).hover(function() {
                // Load the marketing_overlay_hovering cookie to make sure overlay is only displayed once.
                var $hovering_cookie = readCookie('marketing_overlay_hovering');
          
                if (!$hovering_cookie) {
                  createCookie('marketing_overlay_hovering', 1, 7);
                  triggerOverlay(false);
                }
              });
            }
          }
        }
      });
      
      function triggerOverlay(checkdelay) {
        var $overlay_width = settings.marketing_overlay.width + "px";
        
        if (checkdelay) {
          var $delay = settings.marketing_overlay.delay;
          
          if (parseInt($delay)) {
            setTimeout(function() {
              $.colorbox({width:$overlay_width, inline:true, href:"#marketing-overlay"});
            }, parseInt($delay) * 1000);
          }
          
        }
        else {
          $.colorbox({width:$overlay_width, inline:true, href:"#marketing-overlay"});
        }
      }
      
      function addEvent(obj, evt, fn) {
        if (obj.addEventListener) {
          obj.addEventListener(evt, fn, false);
        }
        else if (obj.attachEvent) {
          obj.attachEvent("on" + evt, fn);
        }
      }
      function createCookie(name, value, days) {
        var expires;
    
        if (days) {
          var date = new Date();
          date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
          expires = "; expires=" + date.toGMTString();
        } else {
          expires = "";
        }
        document.cookie = escape(name) + "=" + escape(value) + expires + "; path=/";
      }
    
      function readCookie(name) {
        var nameEQ = escape(name) + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
          var c = ca[i];
          while (c.charAt(0) === ' ') c = c.substring(1, c.length);
          if (c.indexOf(nameEQ) === 0) return unescape(c.substring(nameEQ.length, c.length));
        }
        return null;
      }
    
      function eraseCookie(name) {
        createCookie(name, "", -1);
      }
    }
  };
}(jQuery));

